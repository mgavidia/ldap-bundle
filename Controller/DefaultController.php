<?php

namespace Miami\LdapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/ldap/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }


    /**
     * @Route("/login")
     * @Template()
     */
    public function loginAction(){
        $request = $this->getRequest();
        $session = $request->getSession();
        $error = '';
        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        if ($error) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        $csrfToken = $this->container->has('form.csrf_provider')
            ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate')
            : null;

        $form = $this->createFormBuilder(Null)
            ->add('_username', 'text')
            ->add('_password', 'password')
            ->add('submit', 'submit')
            ->getForm();
        return $this->render('MiamiLdapBundle:Default:login.html.twig', array(
            'error' => $error,
            'form' => $form->createView(),
            'csrf_token' => $csrfToken,
            'last_username' => ''
        ));
    }

    /**
     * @Route("/login_check")
     * @Template()
     */
    public function loginCheckAction(Request $request){

    }
}
