<?php

namespace Miami\LdapBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('miami_ldap')
            ->fixXmlConfig('domain_controller', 'domain_controllers')
            ->children()
                ->variableNode('suffix')
                ->defaultValue('@domain.com')
                ->end()
                ->variableNode('base_dn')
                ->defaultValue('DC=some,DC=domain,DC=com')
                ->end()
                ->arrayNode('domain_controllers')
                    ->prototype('scalar')->end()
                ->end()
                ->variableNode('username')
                ->defaultValue('somename')
                ->end()
                ->variableNode('password')
                ->defaultValue('somepassword')
                ->end()

        ;


        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        /*
         * ldap.suffix: "@law.miami.edu"
        #    ldap.base_dn: "DC=law,DC=miami,DC=edu"
        #    ldap.domain_controllers: ["dc-02.law.miami.edu"]
        #    ldap.username: "sAMAccountName"
        #    ldap.password: "!@home"
         */

        return $treeBuilder;
    }
}
