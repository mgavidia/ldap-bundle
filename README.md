Miami Law LDAP Module for symfony2
==================================

## Installation

Installation is easy:

1. Download with composer
2. Enable the Bundle

### Step 1: Download using composer

Add miami-ldap in your composer.json:

```js
{
        "autoload": {
                "files": ["vendor/adLDAP/adLDAP/src/adLDAP.php"]
        },
        "require": {
                    "miami/ldap-bundle": "dev-master"
                   },
        "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/mgavidia/ldap-bundle.git",
            "version": "dev-master"
        },
        {
            "type": "package",
            "package": {
                "name": "adLDAP/adLDAP",
                "version": "4.0.4",
                "dist": {
                    "url": "http://sourceforge.net/projects/adldap/files/adLDAP/adLDAP_4.0.4/adLDAP_4.0.4r2.zip/download",
                    "type": "zip"
                }
            },
            "target-dir": "vendor/adLDAP"
        }
        ]
}
```
### Step 2: Enable the Bundle

Enable in the kernel:

``` php
<?php
// app/AppKernel.php
public function registerBundles()
{
    $bundles = array(
        // ....
        new Miami\LdapBundle\MiamiLdapBundle(),
    );
}
```

Enable in the routing:

``` yaml
miami_ldap:
    resource: "@MiamiLdapBundle/Controller"
    type:     annotation
    prefix:   /
```
