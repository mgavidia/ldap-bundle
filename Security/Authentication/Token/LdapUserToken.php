<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mgavidia
 * Date: 9/3/13
 * Time: 11:28 AM
 * To change this template use File | Settings | File Templates.
 */

// src/Miami/LdapBundle/Security/Authentication/Token/LdapUserToken.php
namespace Miami\LdapBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class LdapUserToken extends AbstractToken
{
    public $sessionId;
    public $username;
    public $password;
    public $member;
    public $needsAuthentication = true;

    public function __construct(array $roles = array())
    {
        parent::__construct($roles);

        // If the user has roles, consider it authenticated
        $this->setAuthenticated(count($roles) > 0);
    }

    public function getCredentials()
    {
        return '';
    }

    public function getRoles()
    {
        if ($this->getUser()) {
            return $this->getUser()->getRoles();
        } else {
            return array();
        }
    }

    public function isLdapAuthenticated()
    {
        return true; // Left as an exercise
    }
}