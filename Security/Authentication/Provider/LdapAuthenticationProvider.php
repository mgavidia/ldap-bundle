<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mgavidia
 * Date: 9/5/13
 * Time: 5:13 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Miami\LdapBundle\Security\Authentication\Provider;

use Miami\LdapBundle\Security\Authentication\Token\LdapUserToken;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Miami\LdapBundle\Security\Authentication\Token\LdapUserToken as Token;

/**
 * The Provider is the component of the authentication system that authenticates tokens.
 */
class LdapAuthenticationProvider implements AuthenticationProviderInterface
{
    private $container;
    private $userProvider;
    private $encoderFactory;

    /**
     * @param Container $container
     * @param UserProviderInterface $userProvider
     * @param EncoderFactory $encoderFactory
     */
    public function __construct(UserProviderInterface $userProvider, EncoderFactory $encoderFactory, Container $container)
    {
        $this->container = $container;
        $this->userProvider   = $userProvider;
        $this->encoderFactory = $encoderFactory; // usually this is responsible for validating passwords
    }

    /**
     * This function authenticates a passed in token.
     * @param  TokenInterface          $token
     * @return TokenInterface
     * @throws AuthenticationException if wrong password or no username
     */
    public function authenticate(TokenInterface $token)
    {
        if (!empty($token->username)) {
            $options = array(
                'account_suffix'=> $this->container->getParameter('suffix'),
                'base_dn'=>$this->container->getParameter('base_dn'),
                'domain_controllers'=>$this->container->getParameter('domain_controllers'),
                'ad_username'=>$this->container->getParameter('username'),
                'ad_password'=>$this->container->getParameter('password'),
            );
            $ldap = new \adLDAP($options);
            $check = $ldap->authenticate($token->username, $token->password);

            if (!$check){
                // Need to create an account but for now fall through
                throw new AuthenticationException('Bad LDAP credentials.');
            }
            $user    = $this->userProvider->loadUserByUsername($token->username);
            $encoder = $this->encoderFactory->getEncoder($user);
        } else {
            throw new AuthenticationException('No user');
        }

        $token->setUser($user);
        $token->setAuthenticated(true);

        return $token;
    }

    /**
     * @inheritdoc
     * @param  TokenInterface $token
     * @return Boolean
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof LdapUserToken;
    }
}