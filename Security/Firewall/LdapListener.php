<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mgavidia
 * Date: 9/3/13
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */

// src/Miami/LdapBundle/Security/Firewall/LdapListener.php
namespace Miami\LdapBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Miami\LdapBundle\Security\Authentication\Token\LdapUserToken;

class LdapListener implements ListenerInterface
{
    protected $securityContext;
    protected $authenticationManager;

    public function __construct(SecurityContextInterface $securityContext, AuthenticationManagerInterface $authenticationManager)
    {
        $this->securityContext = $securityContext;
        $this->authenticationManager = $authenticationManager;
    }

    public function handle(GetResponseEvent $event)
    {
        throw new \Exception("CRAP");
        $request = $event->getRequest();

        $token = new LdapUserToken();

        // now populate it with whatever information you need, username, password...
        $token->username = $request->get("_username");
        $token->password = $request->get("_password");

        try {
            $authToken = $this->authenticationManager->authenticate($token);
            $this->securityContext->setToken($authToken);

            return;
        } catch (AuthenticationException $failed) {
            // ... you might log something here

            // To deny the authentication clear the token. This will redirect to the login page.
            // Make sure to only clear your token, not those of other authentication listeners.
            //$token = $this->securityContext->getToken();
            //if ($token instanceof LdapUserToken && $this->providerKey === $token->getProviderKey()) {
            //     $this->securityContext->setToken(null);
            //}
            //return;

            // Deny authentication with a '403 Forbidden' HTTP response
            //$response = new Response();
            //$response->setStatusCode(403);
            //$event->setResponse($response);

        }

        // By default deny authorization
        //$response = new Response();
        //$response->setStatusCode(403);
        //$event->setResponse($response);
    }
}