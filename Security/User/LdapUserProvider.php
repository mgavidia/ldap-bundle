<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mgavidia
 * Date: 9/3/13
 * Time: 11:25 AM
 * To change this template use File | Settings | File Templates.
 */

// src/Miami/LdapBundle/Security/User/LdapUserProvider.php
namespace Miami\LdapBundle\Security\User;


use FOS\UserBundle\Model\UserManagerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class LdapUserProvider implements UserProviderInterface
{
    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var Container $container
     */
    protected $container;

    public function __construct(Container $container, UserManagerInterface $userManager)
    {
        $this->container = $container;
        $this->userManager = $userManager;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->userManager->findUserByUsername($username);
        if (!$user){
            /*$options = array(
                'account_suffix'=> $this->container->getParameter('suffix'),
                'base_dn'=>$this->container->getParameter('base_dn'),
                'domain_controllers'=>$this->container->getParameter('domain_controllers'),
                'ad_username'=>$this->container->getParameter('username'),
                'ad_password'=>$this->container->getParameter('password'),
            );

            $token = $this->container->get('security.context')->getToken();
            $password = $token->getAttribute("_password");

            $ldap = new \adLDAP($options);
            $check = $ldap->authenticate($username, $password);

            if (!$check){
                throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
            }

            $new = $this->userManager->createUser();
            $new->setUsername($username);
            $this->userManager->updateUser($new);
            return $new;*/
        }

        throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Acme\WebserviceUserBundle\Security\User\WebserviceUser';
    }
}